---
title: "How the US Destroyed Afghanistan"
date: 2021-08-25
---

> Some 35,000 Muslim radicals, from 40 Islamic countries joined Afghanistan's fight between 
1982-1992...Afghan people don't have a history of being religious zealots. To create the 
CIA-desired jihad required the recruitment of Arab, Egyptian, and Pakistani extremists. So the 
fundamentalism that emerged in Afghanistan is a CIA construct.

-Ahmed Rahid, Journalist

Afghan national identity is a relatively new phenomenon, emerging around the time of the Durrani
Empire (1747-1823), when the whole of the region was practicing Sunni Islam. But even today,
Afghanistan remains a multiethnic and tribalistic society. What is now Afghanistan has been a
war-torn region for centuries, likely due to its strategic location. It is a gateway to the Indian
Subcontinent. Halford Mackinder, a 'founding father' of geopolitics once said,
"Whoever gains control of Central Asia gains control over the Eurasian continent; whoever controls
Eurasia gains control over the world."

<img alt="Afghanistan on a map" src="https://cdn.britannica.com/76/183576-050-FC2A06E2/World-Data-Locator-Map-Afghanistan.jpg" width="500px"/>

*<center>Afghanistan borders Turkmenistan, Uzbekistan, and Tajikistan, all oil-rich countries.
Notably for this post, it also borders Iran, China, and Pakistan.</center>*

### Saudi Arabia and Islamic Extremism

In 1744, Muhammad ibn Abd al-Wahhab, a Sunni religious zealot, formed an alliance with the House of
Saud, the royal family of Saudi Arabia. His fundamentalist interpretation of Islam, Wahhabism,
sought to reestablish the Caliphate, the fusion of state power and religion adopted during the
Muslim expansion. Wahhabism particularly is set apart by its obsession with purging Sufis, Shiites,
and any other Muslims who do not conform to its interpretation of the Quran. Even in modern times,
Saudi Arabia, a close US ally, a [powerful lobby][11] in the US government, and major donor to
the [Clinton Foundation][12], is thought to have spent more than [$100 billion][1]
financing the propagation of Wahhabism. Wahhabism forms the ideological basis of terrorist groups
like al-Qaeda and ISIS. Saudi Arabia is also alleged to provide financial support directly to these
groups. [WikiLeaks][2] has revealed a secret 2009 paper signed by former US Secretary of State,
Hillary Clinton, stating, "More needs to be done since Saudi Arabia remains a critical financial
support base for al-Qaeda, the Taliban, LeT and other terrorist groups."

### The Saur Revolution

For centuries, the vast majority of land in Afghanistan had been owned by a small group of
landlords. In the mid-1960s, revolutionary elements coalesced to form the People's Democratic
Party (PDP). In 1973, the Afghan king was deposed, but the government that took his place proved to
be corrupt and unpopular. In 1978, it was forced out by the Afghan military, who invited the PDP to
form a new government under the Marxist leader, Noor Mohammed Taraki. This event is called the Saur
Revolution and created the Democratic Republic of Afghanistan (DRA), a socialist and Soviet-aligned
state. The DRA proceeded to legalize labor unions, enact a minimum wage, progressive taxation,
literacy campaigns, and economic development. It moved swiftly to end opium poppy cultivation,
as the majority of illicit opiates in the world were produced in Afghanistan. Contrary to what you
might hear from voices in the West, the Soviet Union was not involved in the formation of the
DRA. [John Ryan][3], professor at the University of Winnipeg, who was researching Afghanistan,
said, "It was a totally indigenous happening. Not even the CIA blamed the USSR for it." It also
continued a campaign begun by the deposed king to improve the conditions of Afghan
women. [The World Congress Against Extremism and Takfiri Movements][4], an event in Iran comprising
over 600 Muslim scholars from 80 countries, describes the state of Kabul:

> In the 1980s, Kabul was “a cosmopolitan city. Artists and hippies flocked to the capital.
Women studied agriculture, engineering and business at the city’s university. Afghan women held
government jobs.”  There were female members of parliament, and women drove cars, and travelled
and went on dates, without needing to ask a male guardian for permission.

<img alt="Afghan Women in the 1970s" src="https://i.guim.co.uk/img/media/240d45183279d9b298796310f456981f0b1e1900/0_0_3537_2401/master/3537.jpg?width=620&quality=85&auto=format&fit=max&s=14ff106b6bf1ea01d138ad9bf9f0ec94" width="500px"/>

*<center>Women in Kabul in 1972. Afghan women were steadily gaining rights until the 1990s.
Admittedly, rural Afghanistan was much further behind.</center>*

**CW: Child sexual abuse**  
Notably, the DRA nearly eradicated a centuries old practice among some wealthy and powerful 
men in Afghanistan: bacha bazi (Persian for "boy play"). The custom involves young adolescent boys 
dancing in women's clothing for a crowd, and then raped by an older man. It must be noted, this 
revolting practice is taboo among the Afghan people; one reason for the Taliban's rise in the 1990s 
was their strict opposition to bacha bazi, carrying the death penalty.

### The Afghan Trap

There was serious opposition to the DRA's reforms among the feudal landlords, rural mullahs, and 
tribesmen. The Soviet Union rejected multiple Afghan requests for military aid, citing 
negative political repercussions. Soviet Head of State, Leonid Brezhnev, advised the DRA to slow 
drastic social reforms and to seek broader support among the Afghan population. We know that 
merely a year after the DRA's founding, the United States was already using this opposition to 
destabilize Afghan society. President Jimmy Carter's National Security Advisor, Zbigniew
Brzezinski (interestingly, his daughter, Mika, is a host of Morning Joe on MSNBC), devised a 
plan to lure the Soviet Union into an unwinnable conflict. He admitted this in
[a 1998 interview][5]:

> Question: When the Soviets justified their intervention by asserting that they intended to fight
against secret US involvement in Afghanistan, nobody believed them. However, there was an
element of truth in this. You don’t regret any of this today?

> Brzezinski: Regret what? That secret operation was an excellent idea. It had the effect of drawing
the Russians into the Afghan trap, and you want me to regret it? The day that the Soviets officially
crossed the border, I wrote to President Carter, essentially: “We now have the opportunity of giving
to the USSR its Vietnam war." Indeed, for almost 10 years, Moscow had to carry on a war that was
unsustainable for the regime, a conflict that brought about the demoralization and finally the
breakup of the Soviet empire.

Between 1979 and 1989, the CIA provided weapons, logistical support, and training to the various 
jihadist militant groups, collectively referred to as the Mujahideen. In fact, this operation is 
recognized by Guinness World Records as the [most expensive covert action][6] in history, at $2
billion. By December 1979, the Soviet army intervened in Afghanistan after Taraki was assassinated.
In the 1998 interview, Brzezinski admitted that the CIA had already been aiding the Mujahideen for
6 months:

> Brzezinski: Yes. According to the official version of history, CIA aid to the Mujahideen began 
during 1980, that is to say, after the Soviet army invaded Afghanistan on December 24, 1979. But the
reality, closely guarded until now, is completely otherwise: Indeed, it was July 3, 1979 that
President Carter signed the first directive for secret aid to the opponents of the pro-Soviet
regime in Kabul. And that very day, I wrote a note to the president in which I explained to him
that in my opinion this aid was going to induce a Soviet military intervention.

Notably, the son of a wealthy Saudi Arabian construction mogul joined and funded the Mujahideen: 
Osama bin Laden.

<img alt="Anti-Soviet warrior" src="https://i.insider.com/52a1c37869bedd476f5aaefd?width=700&format=jpeg&auto=webp" width="500px"/>

*<center>Osama bin Laden was praised by western media when he was an anti-Soviet
terrorist.</center>*

<img alt="Original ending of Rambo III" src="https://i.stack.imgur.com/gylUa.jpg" width="500px"/>

*<center>The original ending of Rambo III praised the Mujahideen. It was later changed to "Gallant 
people of Afghanistan."</center>*

Due to the length, cost, and issues at home, the Soviet forces evacuated the country in February 
1989. The DRA continued fighting for another 3 years, outlasting the Soviet Union itself by a 
year. But eventually the DRA was defeated and the Mujahideen took power. In 2001 Amnesty 
International reported that the Mujahideen used sexual assault as "a method of intimidating 
vanquished populations and rewarding soldiers." Throughout the war, opium production [grew to 
staggering levels][7]. 100 tons in the 1970s to 2,000 tons by 1991. A network of heroin 
laboratories opened along the Afghan-Pakistani border. By 1984, this region was supplying 60% of 
the US heroin market. Charles Cogan, former director of the CIA's Afghan operation, remarked in 
a 1995 interview:

> Our main mission was to do as much damage as possible to the Soviets...We didn't really have the
resources or the time to devote to an investigation of the drug trade. I don’t think that we need
to apologize for this...There was fallout in terms of drugs, yes. But the main objective was 
accomplished. The Soviets left Afghanistan.

The Mujahideen also brought back bacha bazi to Afghanistan. A wealthy and powerful Afghan, 
former Mujahideen commander, and bacha bazi practitioner, "Dastager" stated in a
[PBS documentary][8]:

> I was in the battlefield. I did jihad and became a commander. It was against the Russians, a 
terrible war. Bacha bazi comes from Pakistan. When the Russians invaded, the Mujahideen went over
there. When they had nothing to do, they started bacha bazi.

The Mujahideen had always been a fractured coalition of various jihadist groups, and their 
government quickly fell to infighting. The Islamic Emirate of Afghanistan, also called the Taliban,
fought its way to power in 1996. They stopped the factionalism and brought a religious reign of 
terror with their harsh and strict enforcement of Sharia Law. They required men to have untrimmed
beards and women to wear burqas, covering themselves head to toe. Women were barred from education
and the right to work. "Immoral" women were stoned to death. The Taliban did, however, [wipe out
opium cultivation in less than a year][9]. Ideologically, the Taliban is Deobandi Islam. Deobandi
was originally an Islamic revivalist movement in India in the late 1800s, but since the 1970s,
[it has come under the influence of Wahhabism in Afghanistan and Pakistan][10], another 
indication of Saudi Arabian ties to Islamic extremism.

<img alt="Khalis and Reagan" src="https://media.gettyimages.com/photos/afghan-chmn-of-islamic-union-of-mujahedeen-mohammed-younis-khalis-picture-id50361910?s=2048x2048" width="500px"/>

*<center>Mohammad Yunus Khalis, Mujahideen commander, at the White House. He later had a close
relationship with Taliban commanders.</center>*

<img alt="Taliban visits Unocal VP" src="https://www.aljazeera.com/wp-content/uploads/2016/10/99476acef4a4414c9cc47587fe8a53fe_18.jpeg" width="500px"/>

*<center>Members of the Taliban visit Unocal Vice President's home in Texas to discuss [Afghanistan
oil pipeline][15]. Unocal later merged into Chevron.</center>*

### 9/11 and the War in Afghanistan

Although "9/11 Truther" conspiracy theories have been thoroughly debunked, the reality is we still
don't have all the facts about 9/11. The hijackers were 19 men linked to al-Qaeda. Fifteen of them
were from Saudi Arabia, two were from the United Arab Emirates, one was from Lebanon, and one was
from Egypt. None of them were from Afghanistan, and while it is true that the Taliban was harboring
al-Qaeda operatives, they agreed to extradite Osama bin Laden to the United States if the 
bombing of Afghanistan stopped. President Bush [rejected the offer][13]. Moreover, the Congressional
Joint Inquiry into the September 11 Attacks was released in December 2002 with the last 28 pages
redacted. [The 28 pages][14] were finally declassified in 2016, and they show that some 9/11
hijackers had received financial support from individuals connected to the Saudi Arabian government.

Barack Obama promised to end the war by 2014. His plan was to put the war into overdrive by sending
an additional 30,000 troops. [Limb amputations increased by 60% and genital injuries increased
by 90%][16]. Despite this, the plan failed and the US retreated, stopping short of a full 
withdrawal. Donald Trump made similar promises to end the war in his campaign, but once again
escalated the war by sending more troops. In 2017, he dropped on Afghanistan the largest non-nuclear
bomb ever used by the United States. It was lovingly called by western media [the mother of all
bombs][17]. The Quran contains verses that describe Mecca as the "mother of all cities", and because
of this "mother of all" is often used by Arabic speakers to [describe the most prominent of
something][18]. This is no coincidence, calling it the "mother of all bombs" is a vicious 
mockery of the Muslim world.

**CW: Child sexual abuse**  
Once again, US involvement brought back opium and pedophilia. As of 2021, [90% of the world's
heroin is made from opium grown in Afghanistan][19], fueling the opioid epidemic in the United
States. Although it was technically illegal, bacha bazi returned as well. Local government
authorities are either too weak to stop it, or involved in it themselves. US soldiers were [told to
ignore the abuse][20]. Special forces commander Dan Quinn was relieved of his command and was
withdrawn from Afghanistan after he beat up an American-backed militia commander for keeping a boy
chained to his bed as a sex slave.

<img alt="Opium cultivation in Afghanistan (1994-2018)" src="https://ichef.bbci.co.uk/news/976/cpsprodpb/136E5/production/_106598597_afghan_opium_chart-nc.png" width="500px"/>

*<center>The US invasion tripled pre-Taliban cultivation of opium poppy.</center>*

The war has been costly for both sides. From [Brown University][21], the war has cost $2.261 
trillion and killed 241,000 people. 71,000 of which, almost 30%, have been Afghan and Pakistani
civilians. In 2009, the Afghan Ministry of Public Health reported that two-thirds of Afghans are
suffering from mental health problems. The war is also not popular with the people of either
country. In 2020, [three quarters][22] of Americans wanted to end the wars in Afghanistan and Iraq,
and in 2019 [ninety percent][29] of Afghans were in support of peace with the Taliban.

The US has put a lot of work into deceiving the public about the war. The CIA has been working in
Germany and France, NATO members that had a military presence in Afghanistan, to shore up public
support for the war, as revealed by [WikiLeaks][27]. Their strategy included building public
sympathy for Afghan women. [Fake feminism is also a CIA recruiting tactic][28]; imperialism got a 
woke makeover. In 2019, the Washington Post published the ["Afghanistan Papers"][24], a set
of confidential interviews prepared by the Special Inspector General for Afghanistan Reconstruction.
They revealed that high-ranking officials overall were of the opinion that the war was unwinnable,
while intentionally keeping their views from the American public. Some quotes from the interviews:

> So if we are doing such a great job, why does it feel like we are losing?

-Michael Flynn, retired Army lieutenant general

> I have no visibility into who the bad guys are in Afghanistan or Iraq.

> The lack of clarity as to who the enemies are, and what the problems are from an intelligence
standpoint in Afghanistan and Iraq is serious.

-Donald Rumsfeld, former Secretary of Defense

> Lessons Learned Record of Interview: No Long-Term Strategy, No Command and Control

So if the war was incredibly costly, unpopular, and had no long-term strategy, why did it 
continue for so long? Well, it has been an extraordinary success for the military industrial
complex. A 2001 investment in the top five defense contractor stocks (Boeing, Raytheon, Lockheed
Martin, General Dynamics, and Northrop Grumman), by 2021, would yield an [873% return][23],
compared to 517% in an average S&P 500 index fund. There are also an estimated [$1 trillion][25] 
worth of minerals in Afghanistan. There are so many minerals that US officials believed that
Afghanistan could be transformed into one of the most important mining centers in the world.

Like I said at the beginning of the post, Afghanistan has always been in an incredibly 
strategic position geopolitically. Colonel Lawrence Wilkerson gave a [speech for the Ron 
Paul Institute][26], where he said the US is in Afghanistan for three strategic objectives:

1. Afghanistan is the closest place the US has control of that is proximate to China's Belt and 
   Road Initiative, and if it needed to be disrupted, then the US would be able to do so.
2. Afghanistan is near the most "unstable nuclear stockpile in the world" in Pakistan.
3. The US can use Uighurs to destabilize China from an internal, rather than external, place.

Back in July, President Biden was not planning to end to the war; he still wanted to keep [650 
troops][30]. The Taliban took control in 9 days with no shots fired and Biden suddenly changed the 
plan to a complete withdrawal. The Taliban warned the United States, that it would retaliate if 
US forces were in Afghanistan beyond August 31; Biden is [complying with their demands][31]. I think
it is clear: this "withdrawal" is, in reality, a retreat from a lost war.

<img alt="Helicopters in Kabul and Saigon" src="https://i1.wp.com/www.opindia.com/wp-content/uploads/2021/08/IMG_9747.jpg?resize=696%2C392&ssl=1" width="500px"/>

*<center>Left: Withdrawal from Kabul. Right: Withdrawal from Saigon.</center>*

[1]: https://www.huffpost.com/entry/saudi-wahhabism-islam-terrorism_b_6501916
[2]: https://www.theguardian.com/world/2010/dec/05/wikileaks-cables-saudi-terrorist-funding
[3]: https://wsarch.ucr.edu/wsnmail/2001/msg01806.html
[4]: https://makhaterltakfir.com/en/Article/View/10185/From-Afghanistan-to-Syria-Women%E2%80%99s-Rights,-War-Propaganda-and-the-CIA
[5]: https://dgibbs.faculty.arizona.edu/brzezinski_interview
[6]: https://www.guinnessworldrecords.com/world-records/613906-most-expensive-covert-action
[7]: https://www.theguardian.com/news/2018/jan/09/how-the-heroin-trade-explains-the-us-uk-failure-in-afghanistan
[8]: https://www.pbs.org/wgbh/frontline/film/dancingboys/transcript/
[9]: https://www.nytimes.com/2001/05/20/world/taliban-s-ban-on-poppy-a-success-us-aides-say.html
[10]: https://ctc.usma.edu/the-past-and-future-of-deobandi-islam/
[11]: https://en.wikipedia.org/wiki/Saudi_Arabia_lobby_in_the_United_States
[12]: https://www.nytimes.com/interactive/projects/clinton-donors
[13]: https://www.theguardian.com/world/2001/oct/14/afghanistan.terrorism5
[14]: https://web.archive.org/web/20160715183528/http://intelligence.house.gov/sites/intelligence.house.gov/files/documents/declasspart4.pdf
[15]: https://www.aljazeera.com/program/featured-documentaries/2016/10/8/taliban-oil
[16]: https://www.washingtonpost.com/national/amputations-and-genital-injuries-increase-sharply-among-soldiers-in-afghanistan/2011/02/25/ABX0TqN_story.html
[17]: https://www.bbc.com/news/world-asia-39595989
[18]: https://www.businessinsider.com/what-does-mother-of-all-bombs-mean-iraq-saddam-hussein-2017-4
[19]: https://www.bbc.com/news/world-us-canada-47861444
[20]: https://www.nytimes.com/2015/09/21/world/asia/us-soldiers-told-to-ignore-afghan-allies-abuse-of-boys.html
[21]: https://watson.brown.edu/costsofwar/costs/human/civilians/afghan
[22]: https://thehill.com/policy/defense/510851-poll-about-three-quarters-support-bringing-troops-home-from-iraq-afghanistan
[23]: https://theintercept.com/2021/08/16/afghanistan-war-defense-stocks/
[24]: https://www.washingtonpost.com/graphics/2019/investigations/afghanistan-papers/afghanistan-war-confidential-documents/
[25]: https://www.nytimes.com/2010/06/14/world/asia/14minerals.html
[26]: https://youtu.be/91wz5syVNZs?t=1261
[27]: https://wikileaks.org/wiki/CIA_report_into_shoring_up_Afghan_war_support_in_Western_Europe,_11_Mar_2010
[28]: https://www.youtube.com/watch?v=PHckeZoYx04
[29]: https://heartofasia.af/peace-talks-instilled-hopes-in-afghans/
[30]: https://www.politico.com/news/2021/06/24/us-troops-afghanistan-withdrawl-496137
[31]: https://thehill.com/homenews/administration/569172-biden-to-stick-with-aug-31-deadline-for-afghanistan-withdrawal
