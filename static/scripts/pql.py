import csv
import os
import math
import matplotlib.pyplot as plt

# Configuration
MIN_POPULATION = 10_000_000
MIN_PQL = 0
MAX_PQL = 100
regions = [
    {"filename": "appalachia.csv", "name": "Appalachia"},
    {"filename": "black.csv", "name": "Black America"},
    {"filename": "chicano.csv", "name": "Chicano America"},
    {"filename": "indigenous.csv", "name": "Indigenous America"},
]


def get_csv_path(filename):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), "csv", filename)


def calc_pql(literacy_rate, life_expectancy, infant_mortality):
    indexed_life_expectancy = (life_expectancy - 42) * 2.7
    indexed_infant_mortality = (166 - infant_mortality) * 0.625
    return (literacy_rate + indexed_life_expectancy + indexed_infant_mortality) / 3


def calc_mean_county_pql(csv_path, region_name):
    pqls = []
    with open(get_csv_path(csv_path), "r") as file:
        reader = csv.DictReader(file)
        for row in reader:
            pql = calc_pql(
                float(row["literacy_rate"]),
                float(row["life_expectancy"]),
                float(row["infant_mortality"]),
            )
            pqls.append({
                "county": row["county"],
                "pql": pql
            })
    sum = 0
    for pql in pqls:
        sum += pql["pql"]
    return {"country": region_name, "pql": round(sum / len(pqls), 1)}


pqls = []
with open(get_csv_path("countries.csv"), "r") as file:
    reader = csv.DictReader(file)
    for row in reader:
        if int(row["population"]) >= MIN_POPULATION:
            pql = calc_pql(
                float(row["literacy_rate"]),
                float(row["life_expectancy"]),
                float(row["infant_mortality"]),
            )
            pqls.append({
                "country": row["country"],
                "pql": round(pql, 1)
            })

for region in regions:
    region_pql = calc_mean_county_pql(region["filename"], region["name"])
    pqls.append(region_pql)

bar_names = [
    country["country"]
    for country in pqls
    if country["pql"] >= MIN_PQL and country["pql"] < MAX_PQL
][::-1]
bar_values = [
    country["pql"]
    for country in pqls
    if country["pql"] >= MIN_PQL and country["pql"] < MAX_PQL
][::-1]

plt.figure(dpi=1200)
fig, ax = plt.subplots()
bars = ax.barh(bar_names, bar_values, color=(0.0, 0.4, 0.8, 0.6))

scale = MAX_PQL - MIN_PQL
ax.set_xlim(
    math.floor(MIN_PQL / scale) * scale,
    math.ceil(MAX_PQL / scale) * scale
)
ax.set_xticks(range(MIN_PQL, MAX_PQL + 1, 1))
for pos in ["top", "right", "left"]:
    ax.spines[pos].set_visible(False)
ax.spines["bottom"].set_color("#DDDDDD")
ax.tick_params(bottom=False, left=False)
ax.set_axisbelow(True)
ax.xaxis.grid(True, color="#DDDDDD")

bar_color = bars[0].get_facecolor()
for bar in bars:
    ax.text(
        bar.get_width(),
        bar.get_y() + bar.get_height() / 2,
        bar.get_width(),
        verticalalignment="center",
        color=bar_color,
        weight="bold"
    )

fig.tight_layout()
plt.savefig("%s-%s.png" % (MIN_PQL, MAX_PQL))
